provider "aws" {
  region = "us-east-1"
}

data "aws_iam_policy_document" "custom_policy" {
  statement {
    sid     = "EnableRequiredPermissions"
    effect  = "Allow"
    actions = [
      "s3:*",
    ]
    resources = [
      "*",
    ]
  }
}

module "lambda" {
    source                          = "git::https://gitlab.com/ricardocg-tf-aws/tf-lambda-module.git?ref=master"
    name                            = "Lambda-test"
    description                     = "Test"
    filename                        = "./src/lambda.zip"
    handler                         = "lambda.list_s3_buckets"
    runtime                         = "python3.7"
    env                             = "Test"
    iam_policy_doc                  = data.aws_iam_policy_document.custom_policy.json
}


